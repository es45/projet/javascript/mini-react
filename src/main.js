/////////// ROOT ///////////

const root = document.getElementById('root');

/////////// ROOT END ///////////

/////////// PROTOTYPE ///////////

String.prototype.interpolate = function (attributes) {
    return this.replace(/{{(([^}][^}]?|[^}]}?)*)}}/g,
        function (regex, path) {
            let value = attributes.prop_access(path);
            return typeof value === 'string' || typeof value === 'number' ? value : regex;
        }
    );
}

String.prototype.interpolateBis = function () {
    let result = /\{{[^)]*\}}/g.exec(this).map(item => item.substring(2, item.length - 2));
    console.log(result);
    // TODO to finish
}


Object.prototype.prop_access = function (path) {
    let indexes;
    let result;

    if (path === '' || path === null) {
        return this
    } else if (this.valueOf() === null) {
        console.log(path + ' not exist.');
    } else {
        let current_path = '';
        result = this;
        indexes = path.split('.');

        for (let i = 0; i < indexes.length; i++) {
            current_path += indexes[i] + '.';
            result = result[indexes[i]];

            if (result === undefined) {
                current_path = current_path.slice(0, -1);
                break;
            }
        }

        if (result === undefined) {
            console.log(current_path + ' not exist.');
        } else {
            return result;
        }

    }
}

/////////// PROTOTYPE END ///////////

let state = {
    count: 5
}

/////////// PAGE ///////////
function Root() {
    return {
        type: 'div',
        children: [
            Header(),
            {
                type: 'div',
                attributes: {
                    style: 'width: 50%; margin: auto'
                },
                children: [
                    {
                        type: 'h1',
                        attributes: {
                            style: 'font-weight: bold; font-size: 32px; margin-top: 3rem;'
                        },
                        content: 'Home'
                    }
                ]
            }
        ],

    }
}

function Counter() {

    let state = {
        count: 0
    }

    function increment(e) {
        state.count += 1;
        // Rerender on target that contains state.count
        rerender(e.target, state.count)
    }

    return {
        type: 'div',
        children: [
            Header(),
            {
                type: 'div',
                attributes: {
                    id: 'wrapper',
                    style: 'width: 50%; margin: auto; padding: 2rem; margin-top: 2rem;',
                },
                dataset: {
                    background: 'dark'
                },
                children: [
                    {
                        type: 'h1',
                        content: 'Counter !!!'
                    },
                    {
                        type: 'div',
                        attributes: {
                            style: "padding: 1rem 0; display: flex"
                        },
                        props: {
                            count: {
                                default: 0,
                                type: Number
                            }
                        },
                        children: [
                            {
                                type: 'h2',
                                content: 'Counter :'
                            },
                            {
                                type: 'span',
                                content: state.count
                            },
                            {
                                type: 'button',
                                attributes: {
                                    onClick: increment
                                },
                                content: '+1'
                            }
                        ]
                    }
                ]
            }
        ]
    }
}

function Page2() {

    this.state = {
        hidden: true
    }

    return {
        type: 'h1',
        children: [
            Header()
        ]
    }
}

function TodoList() {

    let state = {
        todoList: []
    }

    function addTodo(element) {
        let inputValue = document.querySelector('#todo-input').value;

        if (inputValue !== '' && inputValue == null) return;

        state.todoList.push({
            type: 'li',
            content: inputValue
        });

        inputValue = '';

        console.log(state.todoList);
    }

    return {
        type: 'div',
        children: [
            Header(),
            {
                type: 'div',
                content: 'My Todo List',
                attributes: {
                    id: 'todo-list-wrapper',
                    style: 'width: 50%; margin: auto; padding: 2rem; margin-top: 1rem; border-radius: 1rem; ' +
                        'box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;'
                },
                children: [
                    {
                        type: 'div',
                        attributes: {
                            style: 'padding: 0.5rem'
                        },
                        children: [
                            {
                                type: 'div',
                                children: [
                                    {
                                        type: 'input',
                                        attributes: {
                                            id: 'todo-input',
                                            placeholder: 'Add item to todo list',
                                            style: 'margin-right: 1rem'
                                        },
                                    },
                                    {
                                        type: 'button',
                                        content: 'Add item',
                                        attributes: {
                                            id: 'add-todo',
                                            style: 'padding: 1rem 0.5rem; background-color: white; border: black solid thin; border-radius: 1rem; font-weight: bold',
                                            onClick: addTodo
                                        },
                                    }
                                ]
                            },
                            {
                                type: 'ul',
                                children: [
                                    ...state.todoList
                                ]
                            }
                        ]
                    },
                ]
            }
        ]

    }
}

function Header() {
    return {
        type: 'div',
        attributes: {
            style: 'height: 52px; background-color: rgb(87 83 78); display: flex; justify-content: center;'
        },
        children: [
            {
                type: 'ul',
                attributes: {
                    style: 'list-decoration: none; display: flex'
                },
                children: [
                    {
                        type: 'li',
                        attributes: {
                            style: 'padding: 0 0.5rem; text-decoration: none; color: white; font-weight: 550; display: flex; align-items: center'
                        },
                        children: [
                            {
                                ...link('home', '/'),
                                content: 'Home'
                            }
                        ]
                    },
                    {
                        type: 'li',
                        attributes: {
                            style: 'padding: 0 0.5rem; text-decoration: none; color: white; font-weight: 550; display: flex; align-items: center'
                        },
                        children: [
                            {
                                ...link('counter', '/counter'),
                                content: 'Counter'
                            }
                        ]
                    },
                    {
                        type: 'li',
                        attributes: {
                            style: 'padding: 0 0.5rem; text-decoration: none; color: white; font-weight: 550; display: flex; align-items: center'
                        },
                        children: [
                            {
                                ...link('page2', '/page2'),
                                content: 'Page2'
                            }
                        ]
                    },
                    {
                        type: 'li',
                        attributes: {
                            style: 'padding: 0 0.5rem; text-decoration: none; color: white; font-weight: 550; display: flex; align-items: center'
                        },
                        children: [
                            {
                                ...link('todo', '/todo'),
                                content: 'Todo'
                            }
                        ]
                    }
                ]
            }
        ]
    }
}

function Error() {
    return {
        type: 'h1',
        content: 'Error 404, this route doesn\'t exist',
        attributes: {
            style: "width: 50%; margin: auto; font-weight: bold"
        }
    }
}

/////////// PAGE END ///////////

/////////// EVENT LISTENER ///////////

window.addEventListener('load', (e) => {
    console.log('location', location.pathname);
    this.generatePage();
});

window.addEventListener('popstate', (e) => {
    this.generatePage();
})

/////////// EVENT LISTENER END ///////////

/////////// FUNCTION ///////////

function checkEvent(node, structure) {
    let attr = Object.entries(structure.attributes);
    for (let [attrName, attrValue] of attr) {
        if (/on([A-Z].*)/.test(attrName)) {
            const eventName = attrName.toLowerCase().substring(2, attrName.length);
            node.addEventListener(eventName, attrValue);
        } else {
            node.setAttribute(attrName, attrValue);
        }
    }
    //Parcours les clés et check si préfixée par 'on', trouver l'event correspondant
}

function type_check(variable, conf) {
    let propertyResult = true;
    let typeResult = true;
    let valueResult = true;
    let enumResult = true;

    Object.getOwnPropertyNames(conf).forEach(function (property) {
        switch (property) {
            case 'properties':
                propertyResult = properties_check(variable, conf.properties);
                break;
            case 'type':
                typeResult = type_check_v1(variable, conf.type);
                break;
            case 'value':
                valueResult = value_check(variable, conf.value);
                break;
            case 'enum':
                enumResult = enum_check(variable, conf.enum);
                break;
        }
    });

    return (propertyResult && typeResult && valueResult && enumResult);
}

function properties_check(variable, properties) {
    let result = false;
    Object.keys(properties).forEach(function (value) {
        if (variable[value] !== undefined) {
            result = type_check(variable[value], properties[value]);
        }
    });

    return result;
}

function type_check_v1(variable, type) {
    switch (type) {
        case 'array':
            return Array.isArray(variable);
        case 'null':
            return (variable == null);
        case 'object':
            return (variable.constructor === Object);
        default:
            return (typeof variable === type);
    }
}

function value_check(variable, value) {
    let result = variable === value;
    if (value.constructor === Object) {
        Object.keys(value).forEach(function (key) {
            if (value[key] === variable[key]) {
                result = true;
            }
        })
    }

    return result;
}

function enum_check(variable, enumVal) {
    let result = false;
    enumVal.forEach(function (value) {
        if (value.constructor === Object) {
            Object.keys(value).forEach(function (key) {
                if (value[key] === variable[key]) {
                    result = true;
                }
            })
        } else if (Array.isArray(value)) {
            result = Array.isArray(value) &&
                Array.isArray(variable) &&
                value.length === variable.length &&
                value.every((val, index) => val === variable[index]);

        } else if (value === variable) {
            result = true;
        }
    })

    return result;
}

function checkValidElement() {

}

function link(label, path) {
    return {
        type: 'a',
        attributes: {
            href: path,
            onClick: (e) => {
                e.preventDefault();
                history.pushState({ title: label }, label, `${path}`);
                this.generatePage();
                root.dispatchEvent(new Event('rerender'));
            },
        },
        children: [label]
    };
}

/////////// FUNCTION END ///////////

/////////// GENERATOR ///////////

function generateStructure(structure) {
    const node = document.createElement(structure.type);

    if (structure.attributes) {
        checkEvent(node, structure);
    }

    if (structure.props) {

    }

    if (structure.dataset) {
        node.setAttribute(`data-${Object.keys(structure.dataset)[0]}`, structure.dataset.background);
    }

    if (structure.content) {
        node.appendChild(document.createTextNode(structure.content));
    }

    if (structure.children) {
        for (const child of structure.children) {
            node.appendChild(generateStructure(child))
        }
    }
    root.appendChild(node);
    return node;
}

function generatePage() {

    document.title = history?.state?.title;
    const currentPath = window.location.pathname;
    let elem;
    switch (currentPath) {
        case '/':
            elem = Root();
            break;
        case '/counter':
            elem = Counter();
            break;
        case '/page2':
            elem = Page2();
            break;
        case '/todo':
            elem = TodoList();
            break;
        default:
            elem = Error();
            break;
    }
    if (root.firstChild) {
        root.replaceChild(generateStructure(elem), root.firstChild);
    } else {
        root.appendChild(generateStructure(elem));
    }
}

/////////// GENERATOR END ///////////
function rerender(template, state) {
    console.log(template.nodeType)
    console.log(template)
    template.textContent = state
    // console.log(template)
    // const el = document.querySelector(node);
    // if (!el) return;
    // el.appendChild(template);
}

function render(element, container = root) {
    const dom =
        element.type === 'TEXT_ELEMENT'
            ? document.createTextNode(element.props.nodeValue)
            : document.createElement(element.type)

    Object.keys(element.props).forEach(name => {
        dom.name = element.props.name
    })

    element.props.children.forEach(child => {
        render(child, dom)
    })
}

function createInput() {
    const x = document.createElement('INPUT');
    x.setAttribute('type', 'text');
}

function pushToList(input) {

}

function display() {

}

// function shouldUpdate(oldObj, newObj) {

// }

generatePage();
